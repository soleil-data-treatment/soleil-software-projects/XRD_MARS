"""
This script contains a few useful functions that are used to treat data
"""

import numpy as np
import os
from pymatgen.analysis.diffraction.xrd import XRDCalculator
from pymatgen.core import Structure
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from scipy.special import wofz
from pymatgen.symmetry.groups import SpaceGroup
import torch


def ScatteringVectorList(angles, E0=17):
    """Returns the scattering vectors for corresponding angles (if we desire to plot the intensities in function of Q)
    Args:
        angles (list) : list of angles in degrees
        E0 (float) : level of energy (default = 17 keV)

    Returns:
        Q (list) : list of corresponding scattering vector magnitudes"""
    wavelength = 12.39842 / E0  # Wavelength associated to the X ray at energy E0
    return [(4 * np.pi * np.sin(np.radians(theta)) / wavelength) for theta in angles]


def Voigt(x, crystallite_size, wavelength, theta, K=0.9):
    """Voigt function : convolution of Gaussian and Cauchy-Lorentz distributions.
    Args:
        crystallite_size (float): Size of the crystallite.
        wavelength (float): X-ray wavelength.
        theta (float): Bragg angle.
        K (float, optional): Scherrer constant. Defaults to 0.9.

    Returns:
        Value of Voigt function at position x (float)
    """
    beta = K * wavelength / (crystallite_size * np.cos(theta))
    sigma = beta / (2 * np.sqrt(2 * np.log(2)))
    gamma = beta / 2

    return (
        np.real(wofz((x + 1j * gamma) / sigma / np.sqrt(2)))
        / sigma
        / np.sqrt(2 * np.pi)
    )


def get_space_group_symbol(space_group_number):
    sg = SpaceGroup.from_int_number(space_group_number)
    return sg.symbol


def twotheta_to_q(two_theta, wavelength=0.721):
    """
    Convert two_theta angles to momentum (q) values.

    Args:
    - two_theta (numpy.ndarray or torch.Tensor): Series of angles [deg].
    - wavelength (float): Wavelength value. Default is 0.721.

    Returns:
    - q_values (numpy.ndarray or torch.Tensor): Series of momentum values [Angs-1].
    """
    if isinstance(two_theta, np.ndarray):
        q_values = 4 * np.pi * np.sin(np.deg2rad(two_theta / 2)) / wavelength
    elif isinstance(two_theta, torch.Tensor):
        q_values = 4 * torch.pi * torch.sin(torch.deg2rad(two_theta / 2)) / wavelength
    else:
        raise ValueError(
            "Input type not recognized. Please provide a numpy array or torch tensor."
        )

    return q_values


def q_to_twotheta(q_values, wavelength=0.721):
    """
    Convert momentum (q) to two_theta angles.

    Args:
    - q_values (numpy.ndarray or torch.Tensor): Series of momentum [Angs-1].
    - wavelength (float): Wavelength value. Default is 0.721.

    Returns:
    - two_theta (numpy.ndarray or torch.Tensor): Series of angle values [deg].
    """
    if isinstance(q_values, np.ndarray):
        two_theta = 2 * np.rad2deg(np.asin(q_values * wavelength / 4 / np.pi))
    elif isinstance(q_values, torch.Tensor):
        two_theta = 2 * torch.rad2deg(torch.asin(q_values * wavelength / 4 / torch.pi))
    else:
        raise ValueError(
            "Input type not recognized. Please provide a numpy array or torch tensor."
        )

    return two_theta


def MinMaxScaling(signal):
    """Scales the XRD pattern between 0 and 1 to have the same treatment between data.
    Args:
        signal (list) : list of floats corresponding to the intensity

    Returns:
        normalizedSignal (list) : list of floats between 0 and 1
    """
    min_signal = min(signal)
    max_signal = max(signal)
    if abs(min_signal - max_signal) < 1e-3:
        raise Exception("Difference between min and max is close from zero")
    else:
        return [(x - min_signal) / (max_signal - min_signal) for x in signal]


def calculate_xrd_from_cif(cif_path, crystallite_size, wavelength, K=0.9):
    """
    Calculate the X-ray diffraction (XRD) pattern for a structure given a CIF file and convolve it with a Voigt function.

    Args:
        cif_path (str): Path to the CIF file.
        alpha (float): The Lorentzian component for the Voigt function.
        gamma (float): The Gaussian component for the Voigt function.
        wavelength (str): The type of radiation used for the diffraction. Common choices are 'CuKa' and 'MoKa'.

    Returns:
        (dict): A dictionary with the following keys: 'Formula', 'Angles', 'Intensities', 'Space Group' and 'Crystal System'.
              'Formula' corresponds to the name of the chemical species.
              The 'Angles' and 'Intensities' keys correspond to the calculated XRD pattern convolved with a Voigt function.
              The 'Space Group' key corresponds to the space group of the structure. Idem for 'Crystal System'.
              If an error occurs during the calculation, all keys will have a corresponding value equal to None.
    """
    try:
        structure = Structure.from_file(cif_path)
        formula_name = os.path.splitext(os.path.basename(cif_path))[0]
        calculator = XRDCalculator(wavelength=wavelength)

        sga = SpacegroupAnalyzer(structure)
        conventional_structure = sga.get_conventional_standard_structure()
        space_group = sga.get_space_group_number()
        crystal_system = sga.get_crystal_system()

        dict_crystal_system = {
            "triclinic": 1,
            "monoclinic": 2,
            "orthorhombic": 3,
            "tetragonal": 4,
            "trigonal": 5,
            "hexagonal": 6,
            "cubic": 7,
        }
        crystal_system_id = dict_crystal_system[crystal_system]

        pattern = calculator.get_pattern(conventional_structure)
        q_values = (4 * np.pi / wavelength) * np.sin(np.radians(pattern.x / 2))

        steps = np.linspace(min(q_values), max(q_values), num=10000)
        norm_signal = np.zeros_like(steps)

        for i in range(len(pattern.x)):
            peak_position = q_values[i]
            peak_intensity = pattern.y[i]
            theta = np.radians(pattern.x[i] / 2)

            # Convolve each peak with the provided Voigt profile
            norm_signal += peak_intensity * Voigt(
                steps - peak_position, crystallite_size, wavelength, theta, K
            )

        return {
            "Formula": formula_name,
            "Momentums": steps,
            "Intensities": norm_signal,
            "Space Group": space_group,
            "Crystal System": crystal_system_id,
        }

    except Exception as e:
        print(f"Error processing file {cif_path}: {e}")
        return {
            "Formula": None,
            "Momentums": None,
            "Intensities": None,
            "Space Group": None,
            "Crystal System": None,
        }
