"""
This script regroups a few functions that are used to work with a custom Dataset, which will be the input of our convolutional neural
network. The class inherits from Pytorch's abstract class Dataset, and implements the methods __len__ and __getitem__.
"""

import numpy as np
import pandas as pd
from pymatgen.symmetry.groups import SpaceGroup
from scipy.signal import find_peaks
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import LabelEncoder


class DataSet:
    def __init__(self, file_path: str, q_max: float, min_freq: int = 10) -> None:
        self.data_loader = DataLoader(file_path)
        self.data_preprocessor = None
        self.feature_engineer = None
        self.data_splitter = None
        self.q_max = q_max
        self.min_freq = min_freq
        self.df = None
        self.nb_classes = None

    def load_data(self):
        self.df = self.data_loader.load_data()

    def preprocess_data(self):
        self.data_preprocessor = DataPreprocessor(self.df, self.q_max)
        self.df = self.data_preprocessor.preprocess_data()

    def feature_engineering(self):
        self.feature_engineer = FeatureEngineer(self.df, self.min_freq)
        self.df = self.feature_engineer.feature_engineering(self.q_max)
        self.nb_classes = self.feature_engineer.nb_classes

    def split_data(self, test_size=0.2, random_state=42):
        self.data_splitter = DataSplitter(self.df, test_size, random_state)
        return self.data_splitter.split_data()


class DataLoader:
    def __init__(self, file_path: str) -> None:
        self.file_path = file_path
        self.df = None

    def load_data(self):
        self.df = pd.read_parquet(self.file_path, engine="pyarrow")
        return self.df


class DataPreprocessor:
    def __init__(self, df: pd.DataFrame, q_max: float) -> None:
        self.df = df
        self.q_max = q_max

    def preprocess_data(self):
        self.df["Momentums"] = self.df["Momentums"].apply(
            lambda x: np.array(x)[np.array(x) <= self.q_max]
        )
        self.df["Intensities"] = self.df.apply(
            lambda x: np.array(x["Intensities"])[: len(x["Momentums"])], axis=1
        )
        return self.df


class FeatureEngineer:
    def __init__(self, df: pd.DataFrame, min_freq: int = 10) -> None:
        self.df = df
        self.min_freq = min_freq
        self.nb_classes = None

    def feature_engineering(self, q_max):
        features = []

        for _, row in self.df.iterrows():
            cutoff_indices = row["Momentums"] <= q_max
            momenta_cutoff = row["Momentums"][cutoff_indices]
            intensities_cutoff = row["Intensities"][cutoff_indices]

            peaks, _ = find_peaks(intensities_cutoff)

            feature_dict = {"total_peaks": len(peaks)}

            for i in range(1, 11):
                if i <= len(peaks):
                    feature_dict[f"peak_{i}_pos"] = momenta_cutoff[peaks[i - 1]]
                else:
                    feature_dict[f"peak_{i}_pos"] = np.nan

            features.append(feature_dict)

        features_df = pd.DataFrame(features)
        features_df["target"] = self.df["Space Group"]
        features_df = features_df.dropna()

        class_counts = features_df["target"].value_counts()
        valid_classes = class_counts[class_counts >= self.min_freq].index
        features_df = features_df[features_df["target"].isin(valid_classes)]

        self.nb_classes = features_df["target"].nunique()

        label_encoder = LabelEncoder()
        features_df["target"] = label_encoder.fit_transform(features_df["target"])

        return features_df


class DataSplitter:
    def __init__(self, df: pd.DataFrame, test_size=0.2, random_state=42):
        self.df = df
        self.test_size = test_size
        self.random_state = random_state

    def split_data(self):
        X = self.df.drop("target", axis=1)
        y = self.df["target"]

        sss = StratifiedShuffleSplit(
            n_splits=1, test_size=self.test_size, random_state=self.random_state
        )
        train_index, test_index = next(sss.split(X, y))

        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]

        return X_train, X_test, y_train, y_test
