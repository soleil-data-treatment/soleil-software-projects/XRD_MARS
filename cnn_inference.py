"""
This script proceeds to do inference and measures the time required for it
"""

import argparse
from models.cnn_model import ConvNN
from data.dataset import XRDPatternDataset
import torch
from torch.utils.data import random_split, DataLoader
import time
import pickle
import numpy

# Configure the device (computes on GPU or CPU)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def main(file_path):
    # Instance of the class CNN
    params = {
        "kernels": [100, 50, 25],
        "strides": [5, 5, 2],
        "input_size": 10000,
        "conv_channels": 64,
    }
    output_size = 227

    # Define the hyperparameters
    batch_size = 1
    num_running_processes = 64

    cnn = ConvNN(params, output_size + 1).to(device)
    cnn.load_state_dict(
        torch.load("./models/cnn_optimized_230.pt", map_location=device)
    )
    cnn.eval()
    cnn.double()

    # Load the dataset from the provided file path
    dataset = XRDPatternDataset(file_path)

    trainset, testset = random_split(dataset, [0.75, 0.25])
    testloader = DataLoader(
        testset, batch_size=batch_size, num_workers=num_running_processes
    )

    # Inference on data
    start = time.time()

    with torch.no_grad():
        for angles, intensities, labels in testloader:
            inputs = intensities.to(device)

            print("save intensities as pickle")
            with open("intensities.pkl", "wb") as file:
                pickle.dump(intensities, file)

            numpy.save("angles.npy", angles.numpy())
            numpy.save("intensities.npy", intensities.numpy())

            print("inputs")
            print(inputs)
            print("intensities")
            print(intensities)

            y_pred = cnn(torch.unsqueeze(inputs, 1))

            # Get the top 5 predicted classes
            top5_pred = torch.topk(y_pred, 5, dim=1).indices[0].cpu().tolist()
            print(f"The top 5 candidate groups are: {top5_pred}")

            break

    end = time.time()
    print(f"The time inference is {end - start} seconds.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Inference script for neural net")
    parser.add_argument(
        "file_path", type=str, help="Path to the Parquet file for inference"
    )
    args = parser.parse_args()
    main(args.file_path)
