import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader, random_split
import matplotlib.pyplot as plt
import time
from data.dataset import XRDPatternDataset


class Sampling(nn.Module):
    """
    Sampling layer using reparameterization trick for Variational Autoencoders (VAE).

    This layer takes the mean and log variance of a Gaussian distribution as input and
    samples from the distribution using the reparameterization trick, making it suitable
    for backpropagation.

    Methods:
        forward(z_mean, z_log_var): Performs the sampling operation.
    """

    def __init__(self):
        super(Sampling, self).__init__()

    def forward(self, z_mean, z_log_var):
        batch, dim = z_mean.size()
        epsilon = torch.randn(batch, dim, device=z_mean.device)
        return z_mean + torch.exp(0.5 * z_log_var) * epsilon


class Encoder(nn.Module):
    """
    Encoder module for a Convolutional Variational Autoencoder.

    This module applies a series of convolutional, max pooling, and dropout layers
    to the input to produce a condensed representation.

    Methods:
        forward(x): Processes the input through the convolutional layers and flattens the result.
    """

    def __init__(self, batch_size, nb_classes):
        super(Encoder, self).__init__()
        self.conv_layers = nn.Sequential(
            nn.Conv1d(
                batch_size, 16, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.2),
            nn.Conv1d(
                16, 16, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.2),
            nn.Conv1d(
                16, 32, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.2),
            nn.Conv1d(
                32, 64, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.2),
            nn.Conv1d(
                64, 128, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.2),
            nn.Conv1d(
                128, 256, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.2),
            nn.Conv1d(
                256, 512, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.2),
            nn.Conv1d(
                512, 512, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.LeakyReLU(),
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.4),
            nn.Conv1d(
                512, 64, kernel_size=3, stride=1, padding=1
            ),  # 3x1 Conv, padding=(3-1)//2
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.4),
            nn.Conv1d(
                64, nb_classes, kernel_size=1, stride=1, padding=0
            ),  # 1x1 Conv, no padding
            nn.MaxPool1d(kernel_size=2, stride=2),
            nn.Dropout(0.4),
        )
        self.flatten = nn.Flatten()

    def forward(self, x):
        x = self.conv_layers(x)
        x = self.flatten(x)
        return x


class MLP(nn.Module):
    """
    Multilayer Perceptron (MLP) for generating latent space parameters in a VAE.

    This module takes the output of an encoder, processes it through dense layers, and
    generates parameters for the latent space representation (mean and log variance).

    Methods:
        forward(x): Processes the input through dense layers to produce z_mean, z_log_var, and z.
    """

    def __init__(self, latent_dim, input_dim=9):
        super(MLP, self).__init__()
        self.dense1 = nn.Linear(input_dim, 100)
        self.leaky_relu = nn.LeakyReLU()
        self.dropout = nn.Dropout(0.2)
        self.dense2 = nn.Linear(100, input_dim)
        self.z_mean = nn.Linear(input_dim, latent_dim)
        self.z_log_var = nn.Linear(input_dim, latent_dim)
        self.sampling = Sampling()

    def forward(self, x):
        x = self.leaky_relu(self.dense1(x))
        x = self.dropout(x)
        x = self.leaky_relu(self.dense2(x))
        x = self.dropout(x)
        z_mean = self.z_mean(x)
        z_log_var = self.z_log_var(x)
        z = self.sampling(z_mean, z_log_var)
        return z_mean, z_log_var, z


class Decoder(nn.Module):
    def __init__(self, latent_dim):
        super(Decoder, self).__init__()
        self.latent_dim = latent_dim
        self.decode_layers = nn.Sequential(
            nn.Unflatten(1, (1, latent_dim)),
            nn.Dropout(0.4),
            nn.ConvTranspose1d(1, 64, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.4),
            nn.ConvTranspose1d(64, 512, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(512, 512, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(512, 256, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(256, 256, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(256, 128, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(128, 128, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(128, 64, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(64, 64, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(64, 32, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(32, 32, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(32, 16, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.ConvTranspose1d(16, 16, kernel_size=3, stride=2, padding=1),
            nn.Dropout(0.2),
            nn.Conv1d(16, 1, kernel_size=3, stride=1, padding=1),
            nn.Sigmoid(),
        )

    def forward(self, x):
        x = self.decode_layers(x)
        return x


class VAE(nn.Module):
    """
    Variational Autoencoder (VAE) combining the Encoder, MLP, and Decoder modules.

    This model encodes an input into a latent representation and reconstructs it. It
    also calculates the reconstruction loss and KL divergence for training.

    Methods:
        forward(x): Encodes and decodes the input, returning the reconstruction, mean, and log variance.
        training_step(data, optimizer): Performs a training step including forward pass and backpropagation.
    """

    def __init__(self, encoder, mlp, decoder):
        super(VAE, self).__init__()
        self.encoder = encoder
        self.mlp = mlp
        self.decoder = decoder

    def forward(self, x):
        into_mlp = self.encoder(x)
        z_mean, z_log_var, z = self.mlp(into_mlp)
        reconstruction = self.decoder(z)
        return reconstruction, z_mean, z_log_var

    def training_step(self, data, optimizer):
        # Forward pass
        reconstruction, z_mean, z_log_var = self(data)

        # Reconstruction loss
        reconstruction_loss = F.binary_cross_entropy(
            reconstruction, data, reduction="sum"
        )

        # KL divergence loss
        kl_loss = -0.5 * torch.sum(1 + z_log_var - z_mean.pow(2) - z_log_var.exp())

        # Total loss
        total_loss = reconstruction_loss + kl_loss

        # Backward pass
        optimizer.zero_grad()
        total_loss.backward()
        optimizer.step()

        return {
            "total_loss": total_loss.item(),
            "reconstruction_loss": reconstruction_loss.item(),
            "kl_loss": kl_loss.item(),
        }


def train_and_evaluate_vae(
    train_loader, test_loader, vae, learning_rate, num_epochs, device
):
    """Trains the VAE on training data and evaluates the loss after every epoch.
    Args:
        - train_loader (DataLoader): Training data that can be loaded in batches.
        - test_loader (DataLoader): Test data that can be loaded in batches.
        - vae (VAE): The Variational Autoencoder model.
        - learning_rate (float): Learning rate for optimization.
        - num_epochs (int): Number of iterations of training.
        - device (torch.device): Device on which the calculations are made (CUDA GPU or CPU).
    Returns:
        - trainloss_list (list): List of the training loss values over epochs.
        - testloss_list (list): List of the testing loss values over epochs.
    """
    optimizer = optim.Adam(vae.parameters(), lr=learning_rate)
    trainloss_list = []
    testloss_list = []
    print("Begin training :")

    for epoch in range(1, num_epochs + 1):
        vae.train()
        running_loss = 0.0

        # Training loop
        for _, data, _ in train_loader:
            data = data.float()
            data = data.to(device)
            optimizer.zero_grad()
            reconstruction, z_mean, z_log_var = vae(data)

            # Calculate loss
            reconstruction_loss = F.binary_cross_entropy(
                reconstruction, data, reduction="sum"
            )
            kl_loss = -0.5 * torch.sum(1 + z_log_var - z_mean.pow(2) - z_log_var.exp())
            loss = reconstruction_loss + kl_loss

            # Backward pass
            loss.backward()
            optimizer.step()
            running_loss += loss.item()

        epoch_loss = running_loss / len(train_loader.dataset)
        trainloss_list.append(epoch_loss)
        print(f"Epoch {epoch}/{num_epochs}, Training Loss: {epoch_loss:.4f}")

        # Evaluation loop
        vae.eval()
        test_loss = 0.0
        with torch.no_grad():
            for _, data, _ in test_loader:
                data = data.float()
                data = data.to(device)
                reconstruction, z_mean, z_log_var = vae(data)

                # Calculate loss
                reconstruction_loss = F.binary_cross_entropy(
                    reconstruction, data, reduction="sum"
                )
                kl_loss = -0.5 * torch.sum(
                    1 + z_log_var - z_mean.pow(2) - z_log_var.exp()
                )
                loss = reconstruction_loss + kl_loss

                test_loss += loss.item()

        test_loss /= len(test_loader.dataset)
        testloss_list.append(test_loss)
        print(f"Epoch {epoch}/{num_epochs}, Testing Loss: {test_loss:.4f}")

    print("Finished training and evaluation!")
    return trainloss_list, testloss_list


# Assuming XRDPatternDataset, Encoder, MLP, Decoder, VAE, train_and_evaluate_vae are defined

if __name__ == "__main__":
    # Define the hyperparameters
    batch_size = 82
    learning_rate = 0.0005012297485033033
    num_epochs = 10
    num_running_processes = 64

    start = time.time()

    # Create the dataset
    dataset = XRDPatternDataset("../data/data/pow_xrd2.parquet")

    # Split the raw data into train set and test set
    trainset, testset = random_split(
        dataset, [int(0.75 * len(dataset)), len(dataset) - int(0.75 * len(dataset))]
    )

    # Create data loaders for train and test sets
    trainloader = DataLoader(
        trainset, batch_size=batch_size, num_workers=num_running_processes
    )
    testloader = DataLoader(
        testset, batch_size=batch_size, num_workers=num_running_processes
    )

    # Configure the device (computes on GPU or CPU)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Create instances of the encoder, MLP, and decoder
    latent_dim = 5  # Example latent dimension, adjust as needed
    encoder = Encoder(batch_size, dataset.nb_space_group).to(device)
    mlp = MLP(latent_dim).to(device)
    decoder = Decoder(latent_dim).to(device)

    # Create an instance of VAE
    vae = VAE(encoder, mlp, decoder).to(device)

    # Train the VAE on the training set and evaluate on the test set
    trainloss_list, testloss_list = train_and_evaluate_vae(
        trainloader, testloader, vae, learning_rate, num_epochs, device
    )

    end = time.time()
    print(f"VAE training and evaluation took : {(end - start):.2f} seconds.")

    # Plotting training and testing loss curves
    plt.figure(figsize=(12, 5))

    # Training loss curve
    plt.subplot(1, 2, 1)
    plt.plot(range(1, num_epochs + 1), trainloss_list, ls="-")
    plt.title("Training Loss Curve")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.ylim(bottom=0)

    # Testing loss curve
    plt.subplot(1, 2, 2)
    plt.plot(range(1, num_epochs + 1), testloss_list, ls="-")
    plt.title("Testing Loss Curve")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.ylim(bottom=0)

    plt.tight_layout()
    plt.show()
