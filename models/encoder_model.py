import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, random_split
from data.dataset import XRDPatternDataset
from sklearn.metrics import confusion_matrix


sequence_len = 64
embed_dim = 128
full_len = sequence_len * embed_dim
data_cut = 10005 - full_len
ff_dim = 1024
num_heads = 6
File_Path = "Test_data.h5"
checkpoint_path = "T-encoder_SG.pth"


class PositionalEncoding(nn.Module):
    def __init__(self, sequence_len, embed_dim):
        super().__init__()
        self.pos_emb = nn.Embedding(sequence_len, embed_dim)

    def forward(self, x):
        positions = torch.arange(0, sequence_len, dtype=torch.long).unsqueeze(1)
        pos_embedding = self.pos_emb(positions).permute(1, 0, 2)
        return x + pos_embedding


class TransformerBlock(nn.Module):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1):
        super().__init__()
        self.att = nn.MultiheadAttention(embed_dim=embed_dim, num_heads=num_heads)
        self.ffn = nn.Sequential(
            nn.Linear(embed_dim, ff_dim), nn.ReLU(), nn.Linear(ff_dim, embed_dim)
        )
        self.layernorm1 = nn.LayerNorm(embed_dim)
        self.layernorm2 = nn.LayerNorm(embed_dim)
        self.dropout1 = nn.Dropout(rate)
        self.dropout2 = nn.Dropout(rate)

    def forward(self, x):
        attn_output, _ = self.att(x, x, x)
        attn_output = self.dropout1(attn_output)
        out1 = self.layernorm1(x + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output)
        return self.layernorm2(out1 + ffn_output)


class TEncoder(nn.Module):
    def __init__(self, sequence_len, embed_dim, num_heads, ff_dim):
        super().__init__()
        self.embedding_layer = PositionalEncoding(sequence_len, embed_dim)
        self.transformer_block_1 = TransformerBlock(embed_dim, num_heads, ff_dim)
        self.transformer_block_2 = TransformerBlock(embed_dim, num_heads, ff_dim)
        self.global_avg_pool = nn.AdaptiveAvgPool1d(1)
        self.fc = nn.Sequential(
            nn.Dropout(0.1),
            nn.Linear(embed_dim, 1024),
            nn.ReLU(),
            nn.Dropout(0.1),
            nn.Linear(1024, 230),
        )

    def forward(self, x):
        x = self.embedding_layer(x)
        x = self.transformer_block_1(x)
        x = self.transformer_block_2(x)
        x = x.permute(0, 2, 1)
        x = self.global_avg_pool(x).squeeze(-1)
        x = self.fc(x)
        return x


def train_and_evaluate(
    train_loader, test_loader, model, learning_rate, num_epochs, device
):
    """Trains the TEncoder model on training data and evaluates accuracy every epoch.
    Args:
        - train_loader (DataLoader): Training data that can be loaded in batches.
        - test_loader (DataLoader): Test data that can be loaded in batches.
        - model (TEncoder): The TEncoder model.
        - learning_rate (float): Learning rate for optimization.
        - num_epochs (int): Number of epochs for training.
        - device (torch.device): Device for calculations (CUDA GPU or CPU).
    Returns:
        - trainloss_list (list): List of training loss values over epochs.
        - accuracy_list (list): List of accuracy values over epochs.
    """
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    criterion = nn.CrossEntropyLoss()
    trainloss_list = []
    accuracy_list = []
    print("Begin training :")

    for epoch in range(1, num_epochs + 1):
        running_loss = 0.0
        model.train()

        # Training loop
        for inputs, labels in train_loader:
            inputs, labels = inputs.to(device), labels.to(device)

            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()

        epoch_loss = running_loss / len(train_loader)
        trainloss_list.append(epoch_loss)
        print(f"Epoch {epoch}/{num_epochs}, Loss: {epoch_loss:.4f}")

        # Evaluation loop
        model.eval()
        correct = 0
        count = 0
        y_true = []
        y_pred_list = []
        with torch.no_grad():
            for inputs, labels in test_loader:
                inputs, labels = inputs.to(device), labels.to(device)
                y_pred = model(inputs)
                y_true.extend(labels.cpu().tolist())
                pred_labels = torch.argmax(y_pred, dim=1)
                y_pred_list.extend(pred_labels.cpu().tolist())
                correct += (pred_labels == labels).float().sum()
                count += len(labels)
        accuracy = float(correct / count)
        accuracy_list.append(accuracy)
        print(f"Model accuracy: {accuracy:.2f}")

        if epoch % 5 == 0 or epoch == num_epochs:
            # Build confusion matrix every 5 epochs
            cf_matrix = confusion_matrix(y_true, y_pred_list)
            df_cm = pd.DataFrame(cf_matrix / np.sum(cf_matrix, axis=1)[:, None])
            plt.figure(figsize=(12, 8))
            sns.heatmap(df_cm, annot=True)
            plt.savefig(f"./models/conf_matrix_epoch_{epoch}.png")

    print("Finished training and evaluation!")
    return trainloss_list, accuracy_list


if __name__ == "__main__":
    # Define the hyperparameters
    batch_size = 82
    learning_rate = 0.0005012297485033033
    num_epochs = 10
    num_running_processes = 64

    start = time.time()
    # Create the dataset
    dataset = XRDPatternDataset("../data/data/pow_xrd2.parquet")

    # Split the raw data into train set and test set
    trainset, testset = random_split(
        dataset, [int(0.75 * len(dataset)), len(dataset) - int(0.75 * len(dataset))]
    )

    # Create data loaders for train and test sets
    trainloader = DataLoader(
        trainset, batch_size=batch_size, num_workers=num_running_processes
    )
    testloader = DataLoader(
        testset, batch_size=batch_size, num_workers=num_running_processes
    )

    # Configure the device (computes on GPU or CPU)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Instance of the TEncoder class
    model = TEncoder(sequence_len, embed_dim, num_heads, ff_dim).to(device)
    model = model.double()

    # Train the model on training set and evaluate on test set
    trainloss_list, accuracy_list = train_and_evaluate(
        trainloader, testloader, model, learning_rate, num_epochs, device
    )

    print("Model's state_dict:")
    for param_tensor in model.state_dict():
        print(param_tensor, "\t", model.state_dict()[param_tensor].size())

    end = time.time()

    # Plotting
    plt.figure(figsize=(12, 5))

    # Training loss curve
    plt.subplot(1, 2, 1)
    plt.plot(range(1, num_epochs + 1), trainloss_list, ls="-")
    plt.title("Training loss curve")
    plt.xlabel("Epoch")
    plt.ylabel("Cross-entropy loss")
    plt.ylim(bottom=0)

    # Accuracy curve
    plt.subplot(1, 2, 2)
    plt.plot(range(1, num_epochs + 1), accuracy_list, ls="-")
    plt.title("Accuracy curve on test set")
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy")
    plt.ylim(0, 1)

    plt.tight_layout()
    plt.show()
