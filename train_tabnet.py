# tabnet_train.py
import numpy as np
import pandas as pd
from pytorch_tabnet.tab_model import TabNetClassifier
from data.dataset import DataSet
import torch

# Load and preprocess data
q_max = 20
xrd_file = "./data/pow_xrd2.parquet"
data_processor = DataSet(xrd_file, q_max)
data_processor.load_data()
data_processor.preprocess_data()
data_processor.feature_engineering()

# Split data
X_train, X_test, y_train, y_test = data_processor.split_data()

# Filter dataset
class_counts = y_train.value_counts()
classes_to_keep = class_counts[class_counts >= 10].index
X_train_filtered = X_train[y_train.isin(classes_to_keep)]
y_train_filtered = y_train[y_train.isin(classes_to_keep)]
X_train_filtered.reset_index(drop=True, inplace=True)
y_train_filtered.reset_index(drop=True, inplace=True)

# Drop rows with NaN values
X_train_dropped = X_train_filtered.dropna()
y_train_dropped = y_train_filtered[X_train_dropped.index]

# Train TabNetClassifier
tabnet_model = TabNetClassifier(verbose=0)
tabnet_model.fit(
    X_train_dropped.to_numpy(),
    y_train_dropped.to_numpy(),
    max_epochs=10,
    batch_size=1024,
)

# Save the model
torch.save(tabnet_model.state_dict(), "tabnet_model.pth")
