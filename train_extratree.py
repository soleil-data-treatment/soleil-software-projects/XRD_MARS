import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import roc_curve, auc
from itertools import cycle
from data.dataset import DataSet
import joblib

# Load and preprocess data
q_max = 20
xrd_file = "./data/pow_xrd2.parquet"
data_processor = DataSet(xrd_file, q_max)
data_processor.load_data()
data_processor.preprocess_data()
data_processor.feature_engineering()

# Split data
X_train, X_test, y_train, y_test = data_processor.split_data()


# Function to filter and drop NaN values
def filter_and_drop(df, labels, classes_to_keep):
    filtered = df[labels.isin(classes_to_keep)]
    dropped = filtered.dropna()
    return dropped, labels[filtered.index].dropna()


# Filter dataset for training
class_counts = y_train.value_counts()
classes_to_keep = class_counts[class_counts >= 10].index
X_train_filtered, y_train_filtered = filter_and_drop(X_train, y_train, classes_to_keep)

# Train ExtraTreesClassifier
extra_trees_model = ExtraTreesClassifier(
    n_estimators=500, max_depth=30, max_features=230, n_jobs=-1, random_state=42
)
extra_trees_model.fit(X_train_filtered, y_train_filtered)

# Filter and drop NaN values for test set
X_test_filtered, y_test_filtered = filter_and_drop(X_test, y_test, classes_to_keep)


# Predict probabilities for ROC curve
y_prob = extra_trees_model.predict_proba(X_test_filtered)

# Compute ROC curve and ROC area for each class
n_classes = y_prob.shape[1]
fpr, tpr, roc_auc = dict(), dict(), dict()
for i in range(n_classes):
    fpr[i], tpr[i], _ = roc_curve((y_test_filtered == i).astype(int), y_prob[:, i])
    roc_auc[i] = auc(fpr[i], tpr[i])

# Compute micro-average ROC curve and ROC area
fpr["micro"], tpr["micro"], _ = roc_curve(
    pd.get_dummies(y_test_filtered).values.ravel(), y_prob.ravel()
)
roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

# Plot ROC curve for each class
colors = cycle(
    [
        "blue",
        "red",
        "green",
        "cyan",
        "magenta",
        "yellow",
        "black",
        "pink",
        "lightgreen",
        "lightblue",
        "gray",
        "indigo",
        "orange",
    ]
)
plt.figure(figsize=(10, 8))
for i, color in zip(range(n_classes), colors):
    plt.plot(
        fpr[i],
        tpr[i],
        color=color,
        lw=2,
        label="ROC curve of class {0} (area = {1:0.2f})".format(i, roc_auc[i]),
    )

plt.plot(
    fpr["micro"],
    tpr["micro"],
    color="deeppink",
    linestyle=":",
    linewidth=4,
    label="micro-average ROC curve (area = {0:0.2f})".format(roc_auc["micro"]),
)
plt.plot([0, 1], [0, 1], "k--", lw=2)
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel("False Positive Rate")
plt.ylabel("True Positive Rate")
plt.title("Receiver Operating Characteristic to multi-class")
plt.legend(loc="lower right")
plt.show()

from sklearn.metrics import accuracy_score, precision_score, recall_score

# Assuming y_test_filtered and y_prob are already defined
# Choose a threshold
threshold = 0.6

# Apply threshold to convert probabilities to class predictions
# Assuming binary classification for simplicity; adjust for multiclass
y_pred = (y_prob[:, 1] >= threshold).astype(int)

# Calculate metrics
accuracy = accuracy_score(y_test_filtered, y_pred)
precision = precision_score(y_test_filtered, y_pred)
recall = recall_score(y_test_filtered, y_pred)

print(f"Accuracy: {accuracy}")
print(f"Precision: {precision}")
print(f"Recall: {recall}")

import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

# Assuming y_test_filtered are the true labels and y_pred are the predictions
# Convert probabilities to class predictions if not already done
# threshold = 0.6  # Example threshold
# y_pred = (y_prob[:, 1] >= threshold).astype(int)  # Adjust based on your scenario

# Compute confusion matrix
cm = confusion_matrix(y_test_filtered, y_pred)

# Plot confusion matrix
disp = ConfusionMatrixDisplay(confusion_matrix=cm)
disp.plot(cmap=plt.cm.Blues)
plt.title("Confusion Matrix")
plt.show()

# Save the model
joblib.dump(extra_trees_model, "extra_trees_model.joblib")
