import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset
from data import twotheta_to_q


class XRDInferenceData(Dataset):
    """Class that reads XRD data from a .txt file and returns a concatenated tensor for momentum and intensity."""

    def __init__(self, txt_file):
        """
        Constructor of the class XRDInferenceData.
        Args:
            txt_file (str): .txt file containing momentum and intensity data
        """
        self.data = pd.read_csv(
            txt_file, sep=None, comment="#", header=0, names=["momentum", "intensity"]
        )

    def __len__(self):
        """Returns the number of data points in the .txt file.
        Returns:
            - number of rows in the dataframe (int)
        """
        return len(self.data)

    def __getitem__(self, idx):
        """Access and returns the data at row index idx in the dataframe.
        Args:
            idx (int): row index where to get information
        Returns:
            data (torch.Tensor): A tensor that concatenates both momentum and intensity.
        """
        data_point = self.data.iloc[idx].values  # returns a numpy array of shape [2]
        print(f"data_point type={type(data_point)} array={data_point}")
        return torch.tensor(data_point, dtype=torch.float32)
