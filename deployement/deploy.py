import pandas as pd
import torch
from torch.utils.data import DataLoader

from data import twotheta_to_q
from data_format import XRDInferenceData
from models import ConvNN
import spglib
import argparse


def get_space_group_symbol(space_group_number):
    return spglib.get_spacegroup_type(space_group_number)["international_short"]


def read_input_file(file_path, lambda_val=None):
    """
    Reads the .txt file and returns the momentum and intensities.

    Args:
    - file_path (str): Path to the input .txt file.
    - lambda_val (float, optional): Provided wavelength for conversion. If None, assumes momentum is provided.

    Returns:
    - momentum or angles (np.array): Momentum or angles values.
    - intensities (np.array): Intensity values.
    """

    # Read the file and ignore lines starting with '#'
    data = pd.read_csv(file_path, comment="#", delim_whitespace=True, header=None)

    # If lambda is provided, convert angles to momentum
    if lambda_val:
        data[0] = twotheta_to_q(data[0].values, lambda_val)

    return data[0].values, data[1].values


def load_model(path):
    # Configure the device (computes on GPU or CPU)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Instance of the class CNN
    params = {
        "kernels": [100, 50, 25],
        "strides": [5, 5, 2],
        "input_size": 10000,
        "conv_channels": 64,
    }
    output_size = 230

    # Modify if you want the 230 space groups (7 -> 230)
    cnn = ConvNN(params, output_size + 1)
    cnn.load_state_dict(torch.load(path, map_location=device))
    cnn.eval()
    cnn = cnn.double()
    return cnn


def process_outputs(y_pred):
    # Get the top 5 predicted classes' indices
    top5_pred_indices = torch.topk(y_pred, 5, dim=1).indices

    # Map the indices to their respective labels
    top5_pred_labels = [
        [get_space_group_symbol(idx) for idx in row]
        for row in top5_pred_indices.cpu().tolist()
    ]

    return top5_pred_labels


def handle_predictions(y_pred, top5_pred_labels, filepath="output.html"):
    html_content = """
    <html>
    <head>
        <title>Predictions</title>
    </head>
    <body>
        <h2>Predicted space groups:</h2>
        <table border="1">
            <tr>
                <th>Space Group Number</th>
                <th>Space Group Name</th>
                <th>Probability</th>
            </tr>
    """

    # For each label in top5_pred_labels, add a row to our table
    for i, label in enumerate(top5_pred_labels):
        probability = (
            y_pred[i] * 100
        )  # Assuming y_pred is aligned with top5_pred_labels
        html_content += f"""
            <tr>
                <td>{i + 1}</td>
                <td>{label}</td>
                <td>{probability:.4f}</td>
            </tr>
        """

    html_content += """
        </table>
    </body>
    </html>
    """

    # Write the content to an HTML file
    with open(filepath, "w") as f:
        f.write(html_content)


def main(args):
    # Load model
    model = load_model(args.model_path)

    # Create dataset and dataloader
    dataset = XRDInferenceData(args.txt_file_path)
    dataloader = DataLoader(dataset, batch_size=32, shuffle=False)

    # Configure the device (computes on GPU or CPU)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model.to(device)

    all_predictions = []
    all_probs = []

    with torch.no_grad():
        for batch_idx, q_intensity in enumerate(dataloader):
            q_intensity = q_intensity.to(device)

            # Assuming model accepts momentum and intensity as inputs; modify if necessary
            outputs = model(q_intensity)

            # Process outputs to get top5 labels and probabilities
            top5_pred_labels = process_outputs(outputs)
            probs, _ = torch.topk(outputs, 5, dim=1)

            all_predictions.extend(top5_pred_labels)
            all_probs.extend(probs.cpu().numpy().tolist())

    # Convert to flat list as we expect handle_predictions to process one sample at a time.
    handle_predictions(all_probs, all_predictions, filepath=f"output.html")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Predict space groups from XRD patterns."
    )
    parser.add_argument(
        "--txt_file_path", type=str, required=True, help="Path to the input .txt file."
    )
    parser.add_argument(
        "--model_path", type=str, required=True, help="Path to the trained model."
    )
    args = parser.parse_args()

    main(args)
