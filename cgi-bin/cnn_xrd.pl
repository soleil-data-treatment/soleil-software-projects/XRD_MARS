#!/usr/bin/perl -w

# E. Farhi (c) Synchrotron SOLEIL GPL3

use strict;
use CGI; # see https://metacpan.org/pod/CGI
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use File::Copy;
use File::Path;

# ensure all fatals go to browser during debugging and set-up
# comment this BEGIN block out on production code for security
BEGIN {
    $|=1;
    use CGI::Carp('fatalsToBrowser');
}

# use https://perl.apache.org/docs/2.0/api/Apache2/RequestIO.html
# for flush with CGI
my $r = shift;
if (not $r or not $r->can("rflush")) {
  push @ARGV, $r; # put back into ARGV when not a RequestIO object
}

use constant IS_MOD_PERL => exists $ENV{'MOD_PERL'};
use constant IS_CGI      => IS_MOD_PERL || exists $ENV{'GATEWAY_INTERFACE'};

$CGI::POST_MAX        = 100*1024; # in bytes, default 100kB
$CGI::DISABLE_UPLOADS = 0;        # 1 disables uploads, 0 enables uploads

# see http://honglus.blogspot.com/2010/08/resolving-perl-cgi-buffering-issue.html
#CGI->nph(1);

# CONFIG: adapt values to your needs
my $executable    = "DISPLAY= HWLOC_COMPONENTS=-gl python3 /opt/xrd_cnn/cnn_inference_single.py --cnn_model cnn_optimized_230.pt";    # the name of the executable

# get the form
my $form          = new CGI;
my $error         = "";

# test form consistency
my @fields   = ( 'energy','dataset');

# test if the form has the required fields
for (@fields) {
  if ( not defined $form->param($_) ) {
    $error .= "Incomplete form: fields are missing: $_ ";
  }
}

# get a valid dataset_name (will be a directory)
my $safe_filename_characters    = "a-zA-Z0-9_.-";
my $energy       = $form->param('energy');
my $io_handle    = $form->upload('dataset');
my $dataset_name = $dataset;
$dataset_name    =~ s/[^$safe_filename_characters]//g; # remove illegal characters

# we check that we have a data file
if (not $error) {
  if (not defined $io_handle) {
    $error .= "Did you forget to provide a diffractogram ? Can not get $dataset  (no handle). ";
  }
}

my $yaml = "# YAML\n";
$yaml .= "generated_by: $0 on " . localtime() . "\n";
$yaml .= "date: " . localtime() . "\n";
$yaml .= "user_ip: " . $ENV{'REMOTE_HOST'} . " [" . $ENV{'REMOTE_ADDR'} . "]\n";
foreach my $f (@fields) {
  my $val = $form->param($f);
  if ($val) { $yaml .= "$f: $val\n"; }
}

# launch model in background
if (not $error) {
  my $cmd = "";
  my $wavelength = $energy; # TODO
  $cmd = "$executable  --wavelength $wavelength " . $form->tmpFileName( $io_handle );

  print STDERR "$0: INFO: $cmd\n";
  
  my $res = `$cmd`;
} 

if (not $error) {
  # write the README.html file
  print "<h1>Space-group determination for $dataset_name</h1>\n";
  print "$yaml";
  print "<pre>$res</pre>\n";

  # the 1st argument of CGI is an Apache RequestIO
  if (defined($r) and $r->can("rflush")) { 
    $r->rflush; 
  }
}
if ($error) {
  print $form->header ( ); # wrap up in html
  print "<h1>ERROR starting fluo/diffraction</h1>\n";
  print "<h2>\n";
  print "<p><div style='color:red'>$error</div></p>";
  print "</h2>\n";
  print "</body></html>";
}

# the 1st argument of CGI is an Apache RequestIO
if (defined($r) and $r->can("rflush")) { 
  $r->rflush; 
}

exit;


