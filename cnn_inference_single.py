"""
This script proceeds to do inference and measures the time required for it
"""

import argparse
import torch
import time
import numpy
import os
from models.cnn_model import ConvNN

# Configure the device (computes on GPU or CPU)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def main(cnn_model, intensities):
    """
    main(cnn_model, intensities)
    Inference of the CNN with an input intensity for reference wavelength (0.721).

    Args:
    - cnn_model (str): Path to a CNN Torch model.
    - intensities (float): Intensity values as input to the CNN model.

    Returns:
    - prints the inference results

    """
    # Instance of the class CNN
    params = {
        "kernels": [100, 50, 25],
        "strides": [5, 5, 2],
        "input_size": 10000,
        "conv_channels": 64,
    }
    output_size = 227

    # Define the hyperparameters
    batch_size = 82
    num_running_processes = 64

    cnn = ConvNN(params, output_size + 1).to(device)
    cnn.load_state_dict(torch.load(cnn_model, map_location=device))
    cnn.eval()
    cnn.double()

    # Load the dataset from the provided file path
    # dataset = XRDPatternDataset(file_path)

    # trainset, testset = random_split(dataset, [0.75, 0.25])
    # testloader = DataLoader(testset, batch_size=batch_size, num_workers=num_running_processes)
    # angles      = numpy.load(angles)
    # intensities = numpy.load(intensities)
    intensities = torch.from_numpy(numpy.resize(intensities, (1, len(intensities))))

    # Inference on data
    start = time.time()

    with torch.no_grad():
        print("intensities")
        print(intensities)

        # perform inference for given intensities
        inputs = intensities.to(device)
        y_pred = cnn(torch.unsqueeze(inputs, 1))
        print(y_pred)

        # Get the top 5 predicted classes
        top5_pred = torch.topk(y_pred, 5, dim=1).indices[0].cpu().tolist()
        print(f"The top 5 candidate groups are: {top5_pred}")

    end = time.time()
    print(f"The time inference is {end - start} seconds.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Inference script for neural net")
    parser.add_argument(
        "-c",
        "--cnn_model",
        type=str,
        help="Path to the CNN Torch model.",
        default="models/cnn_optimized_230.pt",
    )
    # training was done with lambda=0.721 Angs
    parser.add_argument(
        "-l", "--wavelength", type=float, help="Wavelength in [Angs].", default=0.721
    )
    parser.add_argument(
        "-q",
        "--column_q",
        type=int,
        help="Index of the Momentum Q column [Angs-1] (0: 1st column, -1: last column).",
        default=None,
    )
    parser.add_argument(
        "-a",
        "--column_angle",
        type=int,
        help="Index of the Angle column [deg] (0: 1st column, -1: last column).",
        default=0,
    )
    parser.add_argument(
        "-i",
        "--column_intensity",
        type=int,
        help="Index of the Intensity column (0: 1st column, -1: last column).",
        default=1,
    )
    parser.add_argument(
        "-s",
        "--skiprows",
        type=int,
        help="Number of rows to skip at the beginning of the data file.",
        default=0,
    )
    parser.add_argument(
        "intensities",
        type=str,
        help="Path to the diffractogram intensities I(angle[deg]) or I(q[Angs-1]), in text/NPY/NPZ format.",
    )
    args = parser.parse_args()

    # read data file
    print(f"Loading {args.intensities}")
    split_tup = os.path.splitext(args.intensities)
    if split_tup[1].lower() == ".npy":
        data = numpy.load(args.intensities)
    elif split_tup[1].lower() == ".npz":
        data = numpy.loadz(args.intensities)
    else:
        # try as a text file
        data = numpy.loadtxt(args.intensities, skiprows=args.skiprows)

    data = numpy.squeeze(data)
    two_theta = None

    # get diffractogram intensity from specified column in file
    if numpy.ndim(data) == 1:
        # assume we have a I(angle) with default angles
        intensities = data
        args.column_q = None
        args.column_angle = None
        two_theta = numpy.arange(start=5.0, stop=90.0, step=(90.0 - 5.0) / 10000)
    else:
        intensities = data[:, args.column_intensity]

    # handle input types
    if args.column_q:
        # convert the Q column to Angle with lambda=0.721
        if args.column_angle:
            print(
                f"WARNING: Ignoring Angle {args.a} column, using specified Momentum {args.q} column index."
            )
        if arg.wavelength:
            print(
                f"WARNING: Ignoring Wavelength argument {args.l}, as the data is asssumed to be a I(q)."
            )

        q_values = data[:, args.column_q]
        two_theta = q_to_twotheta(q_values, wavelength=0.721)
        # crop from 5 to 90 degrees, and rebin in this range

    elif args.column_angle is not None:
        if not args.wavelength:
            raise Exception(
                f"ERROR: You specified an Angle input {args.a}, which requires a wavelentgh to be defined, but is missing."
            )

        two_theta1 = data[:, args.column_angle]
        # angle -> q -> angle
        if args.wavelength != 0.721:
            q_values = twotheta_to_q(two_theta1, wavelength=args.wavelength)
            two_theta = q_to_twotheta(q_values, wavelength=0.721)
        else:
            two_theta = two_theta1

    elif two_theta is None:
        raise Exception("ERROR: You must specify an angle or momentum column.")

    # crop from 5 to 90 degrees, and rebin in this range with 10000 values (CNN input layer)
    if two_theta is not None:
        two_theta5_90 = numpy.arange(start=5.0, stop=90.0, step=(90.0 - 5.0) / 10000)
        intensities = numpy.interp(
            two_theta5_90, two_theta, intensities, left=0, right=0
        )

    # normalize intensities
    intensities = intensities / max(intensities)

    main(args.cnn_model, intensities)
