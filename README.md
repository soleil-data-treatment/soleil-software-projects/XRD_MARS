# SOLEIL MARS Beamline (Summer 2023 internship) : Phase prediction - Space group identification using XRD patterns and Deep Learning methods

## Installation

```
python3 -m venv arrow
pip3 install pyarrow
pip3 install torch
pip3 install pandas
pip3 install pymatgen
pip3 install scikit-learn
pip3 install seaborn
```

or for the inference (deploy)
```
apt install python3-pymatgen python3-seaborn
```

## Usage with the script


Just send the following command using an intensity vector (NPY format), with 10000 angle values.
```
python3 cnn_inference_single.py -c models/cnn_optimized_230.pt  intensities.npy
```

Then you get the output:
```
$ python3 cnn_inference_single.py models/cnn_optimized_230.pt  intensities.npy
intensities
tensor([[16.6998, 16.4299, 15.6476,  ...,  1.4156,  1.3651,  1.2767]],
       dtype=torch.float64)
tensor([[-23.7328,  -6.2616,  -1.4782,   4.7703,   3.1385,   7.3093,  12.2419,
           0.5251, -22.0023, -23.8657, -24.5774, -22.0263, -21.6819, -21.1711,
         -23.1429, -21.1229, -24.7948, -21.1118, -23.4238, -19.3694, -21.6826,
         -23.1150, -25.4492, -20.6166, -23.0972, -22.6354, -25.6727, -18.6687,
         -21.7364, -20.9971, -22.2155, -17.8729, -22.9897, -21.4699, -21.4316,
         -23.3810, -23.1921, -17.1191, -21.8227, -22.3891, -23.4023, -18.9728,
         -21.5144, -21.3381, -18.5691, -22.6243, -20.8090, -21.2527, -22.5447,
         -23.7263, -19.3439, -22.9379, -23.3539, -25.3280, -22.5068, -19.9216,
         -19.2902, -22.5685, -21.8072, -20.6223, -21.7250, -23.6347, -21.3295,
         -23.7431, -23.9590, -21.6666, -22.3056, -18.5197, -20.8056, -22.2512,
         -19.5320, -18.6680, -23.7803, -22.7205, -21.9677, -19.1706, -23.7171,
         -19.9224, -19.0368, -24.4787, -19.5510, -24.0303, -22.6097, -23.1056,
         -22.6588, -20.7455, -23.6786, -20.0486, -21.6186, -23.4033, -18.2797,
         -22.3555, -21.6499, -17.9274, -22.8768, -21.4065, -21.0290, -23.1452,
         -20.7284, -24.8951, -18.0387, -20.9787, -21.8184, -20.4577, -20.1903,
         -23.0804, -23.7746, -22.6091, -20.8873, -21.3369, -19.2451, -19.2648,
         -21.9775, -22.5801, -24.6327, -23.0290, -16.4780, -23.9860, -24.1062,
         -24.8246, -18.2860, -20.5943, -21.3297, -15.6806, -20.1618, -20.1118,
         -22.3643, -20.8667, -20.8829, -20.7210, -19.8378, -25.1108, -23.7446,
         -19.7470, -22.0302, -23.6274, -22.4350, -23.9286, -21.2732, -22.3418,
         -19.2734, -23.5773, -20.9527, -22.0950, -21.5590, -19.4603, -17.5440,
         -23.6504, -21.9783, -17.8964, -22.9959, -20.9818, -20.8530, -20.6521,
         -21.8264, -19.9948, -21.6523, -21.1403, -22.2219, -21.2711, -21.5550,
         -21.5231, -21.9247, -19.6172, -20.1649, -19.3799, -21.5171, -22.6238,
         -16.7041, -21.2050, -24.8811, -19.8411, -22.9092, -18.9324, -16.6112,
         -22.8203, -22.2854, -19.8203, -24.5595, -23.5779, -22.3320, -21.5704,
         -20.9254, -20.5608, -24.5207, -25.3807, -21.3833, -23.3811, -24.7871,
         -23.2213, -22.9083, -22.3615, -21.5162, -21.6106, -21.7122, -21.5852,
         -21.4751, -20.4548, -23.1174, -23.2223, -25.4263, -20.8901, -22.8839,
         -23.0554, -19.7589, -22.1902, -22.9850, -21.7924, -19.1843, -21.0260,
         -20.6724, -22.7355, -18.8007, -19.5221, -22.2507, -24.8219, -18.9332,
         -22.2586, -18.9483, -23.3606, -20.6392, -19.7757, -22.3519, -19.6436,
         -22.6705, -21.8556, -19.5951, -22.9491]], device='cuda:0',
       dtype=torch.float64)
The top 5 candidate groups are: [6, 5, 3, 4, 7]
The time inference is 1.8161237239837646 seconds.

```

## Usage as a web service

Install
```
apt install apache2 libapache2-mod-perl2 
```

Then configure the web server:
```
sudo a2enmod cgi
sudo service apache2 restart
```

Last:
- copy `cgi-bin/cnn_xrd.pl` into `/usr/lib/cgi-bin`
- copy `html/cnn_xrd.html` anywhere inside `/var/www/html`
- copy `cnn_inference_single.py` into `/opt/xrd_cnn`
- copy the directories `models` and `data` into `/opt/xrd_cnn`

## Coments

SOLEIL project to perform phase identification in multiphase compounds at long term, using synthetic XRD powder patterns.

We employ deep learning techniques to highlight such properties, as well as space group identification, using a pre-trained convolutional neural network (CNN). The set of data can be found in ruche/share-temp/XRD_MARS_datasets (CIF files into Parquet dataset).

The project is split in three folders :

- data : Contains all the files (datasets) and scripts to manage/clean/preprocess the data before feeding it to the model.
- models : Contains the scripts that will encapsulate the models employed (Convolutional neural network / Variational auto-encoder) to predict space groups / phases from the XRD patterns
- tests : Contains the scripts that perform tests and experiments with data (Notebooks), to ensure everything works fine at every step of the project.

There is also a file 'requirements.txt', with all the python librairies required to run the project. Before anything, one must type in a command line, at the root of the project :

```bash
pip install -r requirements.txt
```

Every other library needed can be installed using pip in the terminal (one can add it in the requirements file later on). Any code editor can be used (I personnally worked on VSCode, but any Python editor will do the job).

The tasks will require to make computations on a GPU, so a virtual machine is required (re-grades02).

1/ The program begins by getting all of the Materials Project CIF files we can obtain, with their available API. So first, run the Python script (manually or in command line) _fetch_cif.py_ (from the _data_ folder) to regroup the 118399 existing CIF files from Materials Project into a folder _cif_files_.

2/ Then, from this directory, run the Python script _preprocess.py_ to generate the dataset (in a Parquet format), given the CIF files. Generating the whole dataset takes about 6 hours (with 64 GPU cores from the virtual machine), so this might take a bit of time.

3/ Moreover, we feed the data in batches to a learning CNN so that it predicts space groups from chemical species, or their crystal systems. One can appreciate the curves from the Cross-entropy loss and the accuracy over epochs. I also provided a confusion matrix to perform better evaluation. To do the runnning, from the main folder (the one with data, models, and tests folders), run 

```python
python -m models.cnn_model
```

4/ Finally, I measured the time required for the CNN to do one inference. It is between 0.5 and 0.7 seconds on the CPU, which is quite reasonable. To do this, run

```python
python -m models.cnn_inference
```

This summerizes my work for the 10 weeks (July-August 2023) at SOLEIL.


This work is deployed in SOLEIL's GitLab platform so that it can be continued later and improved to perform phase identification in a mixture of powder crystals. But before considering this, a few things should be fixed :

- Perform better data augmentation (peak broadening, add noise to the model, possibly find other data...)
- Propose a model with a variational auto-encoder to compare with the CNN and possibly use for phase identification.
- Compare the CNN's performance with synthetic data and with experimental data (from McXtrace).
- At a longer term, provide a web interface (drag and drop) that takes a signal as input and returns a prediction of the space group / crystal system or the phase in mixture models.


_Teddy ALEXANDRE (Summer 2023 intern) and Anass BELLACHEHAB (Scientific staff)_
